# Демоверсия задания полуфинала олимпиады "Я - профессионал" 2022-2023 по робототехнике
[![Telegram](https://img.shields.io/badge/Telegram-2CA5E0?style=for-the-badge&logo=telegram&logoColor=white)](https://t.me/iprofirobots)    [![Yandex IProfi](https://img.shields.io/badge/yandex-%23FF0000.svg?&style=for-the-badge&logo=yandex&logoColor=white)](https://yandex.ru/profi/profile/?page=contests)  [![Mail](https://custom-icon-badges.demolab.com/badge/-iprofi.robotics@yandex.ru-red?style=for-the-badge&logo=mention&logoColor=white)](mailto:iprofi.robotics@yandex.ru)

---
![scene pic](docs/figures/scene_view.png)

---

Репозиторий содержит ROS-пакет с минимальным *решением* задачи. Участнику следует, модифицируя этот пакет, решить задачу.

## Задача

Сцена включает в себя мобильный манипулятор, способный перемещаться в ограниченном стенами пространстве - складское помещение (далее: склад). Внутри склада разбросаны контейнеры (красные и синие кубики относительно большого размера).

Вам необходимо с использованием доступных сенсоров реализовать алгоритм управления мобильным манипулятором, который позволит собрать разбросанные по складу детали (красные и синие кубики относительно небольшого размера) в красную и синюю области по углам склада. Детали красного цвета, необходимо переместить в область склада с красным полом, синего - в область склада с синим полом.


## Как все работает

Доступны два docker-образа:

- `scene-master-demo` - read-only образ, включающий сцену и робота в gazebo. Образ скачивается из регистра gitlab.
- `problem-master-demo-img` - образ с зависимостями для решения задачи. Образ собирается у вас на компьютере.

Для запуска docker-контейнеров используется инструмент docker-compose. Описание параметров запуска доступно в этом репозитории в файлах:

- `docker-compose.yml ` - если у вас **нет** видеокарты *Nvidia*.
- `docker-compose.nvidia.yml `. - если у вас есть видеокарта от *Nvidia*.

Для запуска используется инструмент docker-compose.

## Установка и настройка окружения
Для настройки окружения необходимо иметь одну из перечисленных операционных систем:
1. Ubuntu 16.04 и старше
2. Windows 10 и старше, с установленным WSL (Не рекомендуется).

Для подготовки окружения необходимо сделать следующее:
1. Установить docker-engine: [Docker Engine](https://docs.docker.com/engine/install/ubuntu/).  
2. Также необходимо установить docker-compose-plugin: [Docker Compose](https://docs.docker.com/compose/install/linux/).  
3. Если вы планируете использовать видеокарту, установите также nviidia-container-toolkit: [Nvidia Container Toolkit](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html)


## Запуск решения
Склонируйте репозиторий в рабочую директорию:

    git clone https://gitlab.com/beerlab/iprofi2023/demo/master.git
    cd master

Перед запуском на Linux выполните следующую команду:

    xhost +local:docker

Для запуска сцены и этого пакета используйте команду:

    docker compose -f docker-compose.yml up

В случае необходимости пересборки используйте флаг `--build`:

    docker compose -f docker-compose.yml up --build

Для получения последней версии сцены(обновления), используейте флаг `--pull always`:

    docker compose -f docker-compose.yml up --build --pull always

---
Для локальной пересборки используйте файлы, содержащие в названии "local"

    docker compose -f docker-compose.local.yml up --build

Или

    docker compose -f docker-compose.nvidia.local.yml up --build
---

В файле `docker-compose.yml` хранится описание параметров запуска сцены и решения. По умолчанию запускается `example_node`

    rosrun master example_node

Для открытия новой bash-сессии используйте команду

    docker exec -it problem_master_demo bash

## Быстрый перезапуск решения
Для быстрого изменения и пересборки отредактируйте необходимые файлы и в соседней вкладке перезапустите сервис решения с помощью команды:

    docker compose restart problem



## Оценка

Оценивается колличество синих и красных кубиков, перевезенный в зоны соответствующего цвета за 5 минут. За каждое столкновение с препятствиями назначается штраф: -0.1 балл. За каждый привезенный кубик нужного цвета назначается +1 балл.

