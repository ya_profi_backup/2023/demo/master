ARG BASE_IMG
# docker build -t problem-img . --build-arg  BASE_IMG=osrf/ros:noetic-desktop-full

FROM ${BASE_IMG}

SHELL ["/bin/bash", "-c"]
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -y ros-noetic-moveit
RUN cd /ros_ws && catkin build

RUN echo "export ROS_NAMESPACE=robot">>~/.bashrc

ENTRYPOINT [ "/bin/bash", "-ci", " cd /ros_ws && catkin build && source devel/setup.bash && roslaunch master start.launch" ]